#!/bin/sh

VERSION=$2
TAR=../sqljet_$VERSION.orig.tar.xz
DIR=sqljet-$VERSION

unzip $3
rm $3

XZ_OPT==--best tar -c -J -v -f $TAR \
    --exclude 'SqlLexer.java' \
    --exclude 'SqlParser.java' \
    --exclude 'Sql.tokens' \
    --exclude 'gradlew*' \
    --exclude 'gradle/wrapper' \
    --exclude '*.tar.gz' \
    --exclude 'prettify.*' \
    $DIR
rm -Rf $DIR
